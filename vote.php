<!DOCTYPE html>
<html>
<head>
	<title>Tentang</title>
	<link rel="stylesheet" type="text/css" href="css/styleSiswaApp.css">
</head>
<body>
	<div id="menu" class="pembungkus-navbar">
		<div class="pembungkus-menu">
			<div class="navbar-beranda">
				<img class="logo-navbar" src="img/logo-etikos.png" alt="logo" height="40px" width="40px">
				<img class="show-hide" src="img/burgericon.png" height="40px" width="40px" onclick="tampil()">
			</div>
			<div id="navbar-menu" class="navbar-menu">
				<div class="pembungkus-navbar-beranda">
					<span class="isi-menu">Beranda</span>
				</div>
				<div class="pembungkus-navbar-menu"  onclick="window.location='vote.php';">
					<span class="isi-menu active">Vote</span>
				</div>
				<div class="pembungkus-navbar-menu" >
					<span class="isi-menu" onclick="window.location='tentang.php';">Tentang</span>
				</div>
				<div class="pembungkus-navbar-menu" >
					<span class="isi-menu" onclick="window.location='bantuan.php';">Bantuan</span>
				</div>		
			</div>
		</div>
		<div class="menu-profile">
			<img class="logo-profile" src="img/logo-etikos.png" alt="logo" height="40px" width="40px">
			<div class="label-profile">Tatag</div>
			<div class="pembungkus-menu-profile">	
				<div class="navbar-profile">
					<a class="isi-profile" href="ubahPwd.php">Ubah Password</a>
				</div>
				<div class="navbar-profile">
					<a class="isi-profile" href="">Logout</a>
				</div>	
			</div>
		</div>
	</div>
	<div class="judul-vote">
		<h1>PEMILIHAN CALON KETUA OSIS SMADA PERIODE 2021 - 2022</h1>
	</div>
	<div class="list-vote">
		<div class="grid-container">
			<?php 
			for ($i=0; $i < 6; $i++) { 
				?>
				<div class="grid-item">
					<div class="isi-vote">
						<img src="img/foto-calon.png" alt="foto" height="160px" width="324px">
						<label class="label-calon">Tono dan Tini</label>
						<input type="submit" name="vote" value="VOTE">
						<label class="label-nourut">01</label>
					</div>
				</div>
				<?php 
			}
			?>
		</div>
	</div>
	<div class="status-pemilihan">
		<h3>STATUS PEMILIHAN : DITUTUP</h3>
	</div>
	<div class="footerSiswa">
		<span>Copyright&copy;2021 ETIKOS</span>
	</div>
	<script type="text/javascript" src="js/scriptSiswaApp.js"></script>
	<script type="text/javascript" src="js/navbar.js"></script>
</body>
</html>