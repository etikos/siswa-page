<!DOCTYPE html>
<html>
<head>
	<title>Tentang</title>
	<link rel="stylesheet" type="text/css" href="css/styleSiswaApp.css">
</head>

<body>
	<div id="menu" class="pembungkus-navbar">
		<div class="pembungkus-menu">
			<div class="navbar-beranda">
				<img class="logo-navbar" src="img/logo-etikos.png" alt="logo" height="40px" width="40px">
				<img class="show-hide" src="img/burgericon.png" height="40px" width="40px" onclick="tampil()">
			</div>
			<div id="navbar-menu" class="navbar-menu">
				<div class="pembungkus-navbar-beranda">
					<span class="isi-menu">Beranda</span>
				</div>
				<div class="pembungkus-navbar-menu"  onclick="window.location='vote.php';">
					<span class="isi-menu">Vote</span>
				</div>
				<div class="pembungkus-navbar-menu" >
					<span class="isi-menu active" onclick="window.location='tentang.php';">Tentang</span>
				</div>
				<div class="pembungkus-navbar-menu" >
					<span class="isi-menu" onclick="window.location='bantuan.php';">Bantuan</span>
				</div>		
			</div>
		</div>
		<div class="menu-profile">
			<img class="logo-profile" src="img/logo-etikos.png" alt="logo" height="40px" width="40px">
			<div class="label-profile">Tatag</div>
			<div class="pembungkus-menu-profile">	
				<div class="navbar-profile">
					<a class="isi-profile" href="ubahPwd.php">Ubah Password</a>
				</div>
				<div class="navbar-profile">
					<a class="isi-profile" href="">Logout</a>
				</div>	
			</div>
		</div>
	</div>

        <div class="pembungkus-tentang">
		<div class="contentTentang">
			<img src="img/icon-tentang.png" height="150" width="150">
			<h2>Tentang Kami</h1>
				<p class="isi-tentang">Website ETIKOS merupakan website e-voting Ketua Osis Smada yang digunakan untuk pemilihan Calon Ketua Osis secara daring agar Siswa dapat melakukan pemilihan Calon Ketua dimanapun dan dapat dilakukan dengan batas waktu tertentu berfokus pada kemudahan Siswa memilih Calon Ketua Osis tanpa menganggu proses belajar mengajar dan dapat dilakukan saat Siswa tidak memiliki kesibukan</p>
				<span><b>email : developmentweb@gmail.com</b></span>
			</div>
		</div>

		<div class="footerSiswa">
			<span>Copyright&copy;2021 ETIKOS</span>
		</div>
		<script type="text/javascript" src="js/scriptSiswaApp.js"></script>
		<script type="text/javascript" src="js/navbar.js"></script>
	</body>
	</html>