function viewPassword(){
	var passwordInput = document.getElementById('password-field');
	var passStatus = document.getElementById('pass-status');

	if (passwordInput.type == 'password'){
		passwordInput.type='text';
		passStatus.className='fa fa-eye-slash';
		passwordInput.classList.add("unhide-pwd");

	}
	else{
		passwordInput.type='password';
		passStatus.className='fa fa-eye';	

	}
}
